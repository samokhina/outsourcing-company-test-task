import {Component} from '@angular/core';
import {NavMenuItem} from '../shared/components/nav-menu/shared/models/nav-menu-item';
import {BaseModule} from './admin-base';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent {
  menu: NavMenuItem[];
  activeMenuItem: NavMenuItem;

  constructor() {
    this.menu = [
      {
        id: 0,
        title: 'Заказать доставку',
        route: '',
        icon: './assets/icons/icon-add-24.png'
      },
      {
        id: 1,
        title: 'Доставки',
        route: '/admin/deliveries',
        icon: './assets/icons/icon-delivery-24.png'
      },
      {
        id: 2,
        title: 'Заявки',
        route: '',
        icon: './assets/icons/icon-records-24.png'
      },
      {
        id: 3,
        title: 'Контакты',
        route: '',
        icon: './assets/icons/icon-contacts-24.png'
      },
      {
        id: 4,
        title: 'Аккаунт',
        route: '',
        icon: './assets/icons/icon-settings-24.png'
      }
    ];
  }

  onActivate(baseModule: BaseModule) {
    this.activeMenuItem = this.menu.filter((navMenuItem: NavMenuItem) => navMenuItem.id === baseModule.id)[0];
  }
}
