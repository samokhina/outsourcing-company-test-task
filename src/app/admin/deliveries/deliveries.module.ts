import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {DeliveriesComponent} from './deliveries.component';
import {DeliveriesRoutingModule} from './deliveries-routing.module';
import {DeliveriesFilterComponent} from './deliveries-filter/deliveries-filter.component';
import {DeliveriesFilterFormModule} from '../shared/components/deliveries-filter-form/deliveries-filter-form.module';
import {DeliveriesListComponent} from './deliveries-list/deliveries-list.component';
import {DeliveryCardModule} from '../shared/components/delivery-card/delivery-card.module';


@NgModule({
  declarations: [DeliveriesComponent, DeliveriesFilterComponent, DeliveriesListComponent],
  imports: [
    CommonModule,
    FormsModule,
    DeliveriesRoutingModule,
    DeliveriesFilterFormModule,
    DeliveryCardModule
  ],
  exports: [DeliveriesComponent]
})
export class DeliveriesModule {
}
