import {Component, Input} from '@angular/core';
import {Delivery} from '../../shared/models/delivery';


@Component({
  selector: 'app-deliveries-list',
  templateUrl: './deliveries-list.component.html'
})
export class DeliveriesListComponent {
  deliveries: Delivery[];

  @Input() set setDeliveries(deliveries: Delivery[]) {
    this.deliveries = deliveries;
  }

  constructor() {
    this.deliveries = [];
  }
}
