import {Component, EventEmitter, Output} from '@angular/core';
import {DeliveriesFilterFormValue} from '../../shared/components/deliveries-filter-form/deliveries-filter-form-value';

@Component({
  selector: 'app-deliveries-filter',
  templateUrl: './deliveries-filter.component.html',
  styleUrls: ['./deliveries-filter.component.scss']
})
export class DeliveriesFilterComponent {
  public deliveriesFilterValue: DeliveriesFilterFormValue;
  public isShowFilter: boolean;
  @Output() searchEmit: EventEmitter<DeliveriesFilterFormValue>;

  constructor() {
    this.isShowFilter = false;
    this.deliveriesFilterValue = new DeliveriesFilterFormValue();
    this.searchEmit = new EventEmitter();
  }

  public toggleShowFilter(): void {
    this.isShowFilter = !this.isShowFilter;
  }

  public searchDeliveries(): void {
    this.searchEmit.emit(this.deliveriesFilterValue);
  }

  public clearFilter(): void {
    this.deliveriesFilterValue = new DeliveriesFilterFormValue();
  }
}
