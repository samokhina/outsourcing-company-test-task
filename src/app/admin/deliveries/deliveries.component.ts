import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, concat, Observable, of} from 'rxjs';
import {DeliveriesFilterFormValue} from '../shared/components/deliveries-filter-form/deliveries-filter-form-value';
import {Delivery} from '../shared/models/delivery';
import {DeliveryService} from '../shared/services/delivery.service';
import {BaseModule} from '../admin-base';
import {switchMap, tap} from 'rxjs/operators';

@Component({
  selector: 'app-deliveries',
  templateUrl: './deliveries.component.html',
  styleUrls: ['./deliveries.component.scss'],
})
export class DeliveriesComponent extends BaseModule implements OnInit {
  loading$: Observable<boolean>;
  deliveries$: Observable<Delivery[]>;

  private loading: BehaviorSubject<boolean>;
  private readonly searchValueEvent: BehaviorSubject<DeliveriesFilterFormValue>;

  constructor(private deliveryService: DeliveryService) {
    super();
    this.id = 1;
    this.loading = new BehaviorSubject(true);
    this.searchValueEvent = new BehaviorSubject(null);
  }

  ngOnInit(): void {
    this.loading$ = this.loading.asObservable();
    this.deliveries$ = this.searchValueEvent.asObservable().pipe(
      switchMap((searchValue: DeliveriesFilterFormValue) => {
        this.loading.next(true);
        return concat(of(null), this.getDeliveries(searchValue));
      })
    );
  }

  public searchDeliveries(searchValue: DeliveriesFilterFormValue): void {
    this.searchValueEvent.next(searchValue);
  }

  private getDeliveries(searchValue: DeliveriesFilterFormValue = null): Observable<Delivery[]> {
    const deliveries$ = searchValue && searchValue.name
      ? this.deliveryService.getDeliveriesByName(searchValue.name)
      : this.deliveryService.getDeliveries();
    return deliveries$.pipe(tap(() => this.loading.next(false)));
  }
}

