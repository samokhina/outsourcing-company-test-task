import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavMenuModule} from '../shared/components/nav-menu/nav-menu.module';
import {AdminComponent} from './admin.component';
import {AdminRoutingModule} from './admin-routing.module';
import {DeliveryService} from './shared/services/delivery.service';


@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    NavMenuModule
  ],
  providers: [DeliveryService],
  exports: [AdminComponent]
})
export class AdminModule {
}
