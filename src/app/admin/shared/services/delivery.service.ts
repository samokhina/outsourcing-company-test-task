import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {plainToClass} from 'class-transformer';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {environment} from '../../../../environments/environment';
import {BaseService} from '../../../shared/services/base.service';
import {Delivery} from '../models/delivery';


@Injectable()
export class DeliveryService extends BaseService {
  private url = environment.apiURL + '/deliveries';

  constructor(public http: HttpClient) {
    super(http);
  }

  getDeliveries(): Observable<Delivery[]> {
    return this.get(this.url, null).pipe(
      map(
        (deliveries) => plainToClass<any, any>(Delivery, deliveries),
        catchError(() => of([]))
      )
    );
  }

  getDeliveriesByName(name: string): Observable<Delivery[]> {
    return this.get(this.url + `?description.name=${name}`, null).pipe(
      map((deliveries) => plainToClass<any, any>(Delivery, deliveries),
      catchError(() => of([]))
    ));
  }
}
