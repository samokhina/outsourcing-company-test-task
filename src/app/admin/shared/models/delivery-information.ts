export class DeliveryInformation {
  number: string;
  date: string;
  time: string;
}
