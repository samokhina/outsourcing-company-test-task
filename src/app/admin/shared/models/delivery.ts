import {DeliveryDescription} from './delivery-description';

export class Delivery {
  sender: string;
  recipient: string;
  weight: number;
  price: number;
  description: DeliveryDescription;
  otherDeliveries: Delivery[];
}






