import {DeliveryInformation} from './delivery-information';

export class DeliveryDescription {
  status: string;
  name: string;
  information: DeliveryInformation;
}
