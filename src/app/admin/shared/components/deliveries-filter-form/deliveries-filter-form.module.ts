import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DeliveriesFilterFormComponent} from './deliveries-filter-form.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [DeliveriesFilterFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [DeliveriesFilterFormComponent]
})
export class DeliveriesFilterFormModule {
}
