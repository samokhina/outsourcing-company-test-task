import {FormControl, FormGroup} from '@angular/forms';
import {DeliveriesFilterFormValue} from './deliveries-filter-form-value';

export class DeliveriesFilterFormGroup extends FormGroup {
  constructor(registrationFormValue: DeliveriesFilterFormValue = null) {
    super({
      dateStart: new FormControl(
        registrationFormValue && registrationFormValue.dateStart
          ? registrationFormValue.dateStart
          : new Date()
      ),
      dateEnd: new FormControl(
        registrationFormValue && registrationFormValue.dateEnd
          ? registrationFormValue.dateEnd
          : new Date()
      ),
      outsideId: new FormControl(
        registrationFormValue && registrationFormValue.outsideId
          ? registrationFormValue.outsideId
          : ''
      ),
      deliveryId: new FormControl(
        registrationFormValue && registrationFormValue.deliveryId
          ? registrationFormValue.deliveryId
          : ''
      ),
      groupId: new FormControl(
        registrationFormValue && registrationFormValue.groupId
          ? registrationFormValue.groupId
          : ''
      ),
      name: new FormControl(
        registrationFormValue && registrationFormValue.name
          ? registrationFormValue.name
          : ''
      )
    });
  }
}
