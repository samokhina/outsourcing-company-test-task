export class DeliveriesFilterFormValue {
  // Начальная дата
  public dateStart: Date;

  // Конечная дата
  public dateEnd: Date;

  // Внешний ID
  public outsideId: string;

  // ID доставки
  public deliveryId: string;

  // ID группы
  public groupId: string;

  // Наименование
  public name: string;

  constructor() {
    this.dateStart = new Date();
    this.dateEnd = new Date();
    this.outsideId = '';
    this.deliveryId = '';
    this.groupId = '';
    this.name = '';
  }
}
