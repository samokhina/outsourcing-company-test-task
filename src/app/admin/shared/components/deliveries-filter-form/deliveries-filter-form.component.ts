import {Component, EventEmitter, forwardRef, OnDestroy, OnInit, Output} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';
import {DeliveriesFilterFormGroup} from './deliveries-filter-form-group';
import {DeliveriesFilterFormValue} from './deliveries-filter-form-value';

export const DELIVERIES_FILTER_FORM_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DeliveriesFilterFormComponent),
  multi: true,
};

@Component({
  selector: 'app-deliveries-filter-form',
  templateUrl: './deliveries-filter-form.component.html',
  styleUrls: ['./deliveries-filter-form.component.scss'],
  providers: [DELIVERIES_FILTER_FORM_VALUE_ACCESSOR],
})
export class DeliveriesFilterFormComponent implements OnInit, OnDestroy, ControlValueAccessor {
  public deliveriesFilterForm: DeliveriesFilterFormGroup;
  private unsubscribe: Subject<void>;

  public onChange: any = () => {};
  public onTouched: any = () => {};

  @Output() searchEmit: EventEmitter<void>;

  constructor() {
    this.unsubscribe = new Subject();
    this.searchEmit = new EventEmitter();
  }

  get value(): DeliveriesFilterFormValue {
    return this.deliveriesFilterForm.getRawValue();
  }

  set value(value: DeliveriesFilterFormValue) {
    this.deliveriesFilterForm.setValue(value);
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public ngOnInit(): void {
    this.deliveriesFilterForm = new DeliveriesFilterFormGroup();
    this.valueFormChanges();
  }

  public writeValue(value: DeliveriesFilterFormValue): void {
    if (value) {
      this.value = value;
    }
  }

  public setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.deliveriesFilterForm.disable();
    }
  }

  private valueFormChanges(): Subscription {
    return this.deliveriesFilterForm.valueChanges.pipe(takeUntil(this.unsubscribe)).subscribe(() => {
      this.onChange(this.value);
    });
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  public searchDeliveries(): void {
    this.searchEmit.emit();
  }
}
