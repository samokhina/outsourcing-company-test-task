import {Component, HostListener, Input, OnInit} from '@angular/core';
import {TimelineStatusEnum} from '../../../../shared/components/timeline-vertical/shared/timeline-status.enum';
import {Delivery} from '../../models/delivery';

@Component({
  selector: 'app-delivery-card',
  templateUrl: './delivery-card.component.html',
  styleUrls: ['./delivery-card.component.scss'],
})
export class DeliveryCardComponent implements OnInit {
  @Input() delivery: Delivery;
  @Input() isLastDelivery: boolean;
  isMobileView: boolean;
  timelineStatusEnum: typeof TimelineStatusEnum = TimelineStatusEnum;

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.isMobileView = event.target.innerWidth <= 576;
  }

  ngOnInit() {
    this.isMobileView = document.body.clientWidth <= 576;
  }
}
