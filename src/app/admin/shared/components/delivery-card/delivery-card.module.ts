import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DeliveryCardComponent} from './delivery-card.component';
import {TimelineVerticalModule} from '../../../../shared/components/timeline-vertical/timeline-vertical.module';


@NgModule({
  declarations: [DeliveryCardComponent],
    imports: [
        CommonModule,
        TimelineVerticalModule
    ],
  exports: [DeliveryCardComponent]
})
export class DeliveryCardModule {
}
