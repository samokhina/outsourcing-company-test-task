export class NavMenuItem {
  id: number;
  title: string;
  route: string;
  icon: string;
}
