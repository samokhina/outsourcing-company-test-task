import {Component, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {NavMenuItem} from './shared/models/nav-menu-item';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss'],
})
export class NavMenuComponent {
  @Input() menu: NavMenuItem[];
  @Input() activeMenuItem: NavMenuItem;
  @Input() isAdmin: boolean;
  @Output() activeMenuEmit: EventEmitter<boolean>;
  isActiveMenu: boolean;
  isMobileView: boolean;

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.isMobileView = event.target.innerWidth <= 576;
    this.isActiveMenu = !this.isMobileView;
  }

  constructor() {
    this.isAdmin = false;
    this.isMobileView = document.body.clientWidth <= 576;
    this.isActiveMenu = !this.isMobileView;
    this.activeMenuEmit = new EventEmitter(false);
  }

  toggleTopMenu(): void {
    this.isActiveMenu = !this.isActiveMenu;
    this.activeMenuEmit.emit(this.isActiveMenu);
  }

  clickMenuItem(): void {
    if (this.isMobileView) {
      this.toggleTopMenu();
    }
  }
}
