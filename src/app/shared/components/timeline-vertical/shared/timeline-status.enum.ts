export enum TimelineStatusEnum {
  Default = 0,
  First = 1,
  Middle = 2,
  Last = 3,
  Empty = 4,
}
