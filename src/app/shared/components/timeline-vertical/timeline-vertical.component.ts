import {Component, Input} from '@angular/core';
import {TimelineStatusEnum} from './shared/timeline-status.enum';


@Component({
  selector: 'app-timeline-vertical',
  templateUrl: './timeline-vertical.component.html',
  styleUrls: ['./timeline-vertical.component.scss'],
})
export class TimelineVerticalComponent {
  @Input() timelineStatus: TimelineStatusEnum;
  timelineStatusEnum: typeof TimelineStatusEnum = TimelineStatusEnum;
}
