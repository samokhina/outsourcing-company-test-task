import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineVerticalComponent } from './timeline-vertical.component';



@NgModule({
  declarations: [TimelineVerticalComponent],
  exports: [
    TimelineVerticalComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TimelineVerticalModule { }
